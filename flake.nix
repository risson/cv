{
  # name = "cv";

  description = "risson's CV";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (futils.lib) eachDefaultSystem defaultSystems;
      inherit (lib) recursiveUpdate genAttrs substring;

      nixpkgsFor = genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [ self.overlay ];
      });

      version = "${substring 0 8 (self.lastModifiedDate or self.lastModified or "19700101")}_${self.shortRev or "dirty"}";
    in
    recursiveUpdate
    {
      overlay = final: prev: {
        cv = final.stdenvNoCC.mkDerivation {
          pname = "cv";
          inherit version;
          src = self;

          buildInputs = with final; [
            (texlive.combine {
              inherit (texlive)
                scheme-medium
                academicons
                arydshln
                fontawesome5
                moderncv
                multirow
                ;
            })
          ];

          buildPhase = ''
            pdflatex cv-en.tex
            pdflatex cv-en.tex
            pdflatex cv-en.tex

            pdflatex cv-fr.tex
            pdflatex cv-fr.tex
            pdflatex cv-fr.tex
          '';

          installPhase = ''
            install -Dm644 cv-en.pdf $out/share/cv-en.pdf
            install -Dm644 cv-fr.pdf $out/share/cv-fr.pdf
          '';
        };
      };
    }
    (eachDefaultSystem (system:
      let
        pkgs = nixpkgsFor.${system};
      in
      {
        devShell = pkgs.mkShell {
          inputsFrom = [ self.packages.${system}.cv ];
        };

        packages = {
          inherit (pkgs) cv;
        };

        defaultPackage = self.packages.${system}.cv;
      }
    ));
}
